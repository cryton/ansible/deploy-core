# Ansible role - Deploy Core
This role provides installation of Cryton Core using Docker or pip.  
First, it checks if the appropriate user is created, installs Docker or pip and then installs the app.

## Supported platforms

| Platform | version   |
|----------|-----------|
| Debian   | \>=11     |
| Kali     | \>=2022.1 |

## Requirements

- Root access (specify `become` directive as a global or while invoking the role)
    ```yaml
    become: yes
    ```
- Ansible variables (do not disable `gather_facts` directive)

## Parameters
The parameters and their defaults can be found in the `defaults/main.yml` file.

It is recommended to update the `cryton_core_environment` variable.

For example:
```yaml
      cryton_core_environment:
        MY_VARIABLE: my_value
```

| variable                                      | description                                                                                                                                                        |
|-----------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| cryton_core_git_repository                    | Origin repository.                                                                                                                                                 |
| cryton_core_repository_branch                 | Which version to use.                                                                                                                                              |
| cryton_core_version                           | Which version to use.                                                                                                                                              |
| run_as_user                                   | Which user to use/create.                                                                                                                                          |
| cryton_core_app_directory                     | App directory path.                                                                                                                                                |
| cryton_core_executable_directory              | Path to the directory which contains the executable.                                                                                                               |
| cryton_core_installation                      | Type of the installation. Possible options are `pip` and `docker`.                                                                                                 |
| cryton_core_environment                       | Dictionary with the settings. More information can be found [here](https://cryton.gitlab-pages.ics.muni.cz/cryton-documentation/latest/components/core/#settings). |
| cryton_core_output_file                       | Path to the file in which we will store the program output.                                                                                                        |
| cryton_core_command_options                   | Options to use when starting the app using pip.                                                                                                                    |
| cryton_core_prerequisites_docker_compose_file | Name of the Compose file with the prerequisites.                                                                                                                   |
| cryton_core_docker_compose_file               | Name of the Compose file with the app and its prerequisites.                                                                                                       |

## Examples

### Usage
**Pip installation:**
```yaml
  roles:
    - role: deploy-core
      become: yes

```

**Docker installation:**
```yaml
  roles:
    - role: deploy-core
      become: yes
      run_as_user: root
      cryton_core_installation: docker
      cryton_core_environment:
        CRYTON_CORE_RABBIT_HOST: cryton-rabbit
        CRYTON_CORE_DB_HOST: cryton-pgbouncer
        CRYTON_CORE_API_USE_STATIC_FILES: true

```

### Inclusion

[https://docs.ansible.com/ansible/latest/galaxy/user_guide.html#dependencies](https://docs.ansible.com/ansible/latest/galaxy/user_guide.html#dependencies)
```yaml
- name: deploy-core
  src: https://gitlab.ics.muni.cz/cryton/ansible/deploy-core.git
  version: "master"
  scm: git
```
